package com.example.foodsampleapp.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "food_table")
data class FoodItem(
    @PrimaryKey
    val foodName: String,
    val price: String?,
    val foodType: String?,
    var orderQty: Int?
)