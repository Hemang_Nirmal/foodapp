package com.example.foodsampleapp.model

data class Categories (val name:String, var selected:Boolean, val count:Int)