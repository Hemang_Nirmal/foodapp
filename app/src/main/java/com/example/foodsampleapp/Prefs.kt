package com.example.foodsampleapp

import android.content.Context
import android.content.SharedPreferences

class Prefs (context: Context){
    val PREFS_FILENAME = "com.example.foodsampleapp.prefs"
    val IS_DB_POPULATED = "is_db_populated"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    var isDbPopulated: Boolean
        get() = prefs.getBoolean(IS_DB_POPULATED, false)
        set(value) = prefs.edit().putBoolean(IS_DB_POPULATED, value).apply()
}