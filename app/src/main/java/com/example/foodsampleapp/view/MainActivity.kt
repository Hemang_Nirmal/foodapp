package com.example.foodsampleapp.view

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.example.foodsampleapp.R
import com.example.foodsampleapp.adapter.CategoryAdapter
import com.example.foodsampleapp.viewModel.FoodItemViewModel
import com.example.foodsampleapp.adapter.FoodAdapter
import com.example.foodsampleapp.model.Categories
import com.example.foodsampleapp.model.FoodItem
import android.R.attr.bottom
import android.R.attr.right
import android.R.attr.top
import android.R.attr.left
import android.graphics.Rect


class MainActivity : AppCompatActivity(), View.OnClickListener{
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: FoodAdapter
    private lateinit var categoryAdapter: CategoryAdapter
    val foodItems: ArrayList<FoodItem> = ArrayList()
    val categoryItems: ArrayList<Categories> = ArrayList()
    private lateinit var foodItemViewModel: FoodItemViewModel
    private var categoryPopup: PopupWindow? = null
    var categoryMain:Categories? =null
    var categoryStarter:Categories? =null
    var categoryDessert:Categories? =null
    var categoryDrink:Categories? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar_layout.setTitleEnabled(false);
        toolbar_title.text ="Inka Restaurant";
        foodItemViewModel = ViewModelProviders.of(this).get(FoodItemViewModel::class.java)

        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = linearLayoutManager
        adapter = FoodAdapter(foodItems, this)
        recyclerView.adapter = adapter

//        fetchFoodsByCategory()

        foodItemViewModel.allFoodItems.observe(this, Observer { foods ->
            // Update the cached copy of the words in the adapter.
            foods?.let {
                Log.d("Observer Clicked","Observer called")
                FoodAdapter.SHOW_LESS = false
                foodItems.clear()
                foodItems.addAll(it)
                adapter.notifyDataSetChanged()
            }
        })

        foodItemViewModel.allOrderedItems.observe(this, Observer { orders ->
            orders?.let {
                Log.d("Order Observer Clicked","Observer called")
                var count:Int = 0
                orders.iterator().forEach {
                    val qty = it.orderQty
                    if (qty != null) {
                        count = count.plus(qty)
                    }
                }
                updateCartCount(count)
            }
        })

        foodItemViewModel.starterCount.observe(this, Observer {
            categoryStarter = Categories("Starter",false, it)
        })

        foodItemViewModel.mainCount.observe(this, Observer {
            categoryMain = Categories("Main Course",false, it)
        })

        foodItemViewModel.dessertCount.observe(this, Observer {
            categoryDessert = Categories("Dessert",false, it)
        })

        foodItemViewModel.drinkCount.observe(this, Observer {
            categoryDrink = Categories("Drink",false, it)
        })



        btn_menu.setOnClickListener(View.OnClickListener {
            showMenu(it)
        })

        ll_bottom.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, MyCartActivity::class.java)
            startActivity(intent)
        })

        btn_back.setOnClickListener(View.OnClickListener {
            this.finish()
        })

    }

    private fun showMenu(view: View?) {
        categoryItems.clear()
        categoryStarter?.let { categoryItems.add(it) }
        categoryMain?.let { categoryItems.add(it) }
        categoryDessert?.let { categoryItems.add(it) }
        categoryDrink?.let { categoryItems.add(it) }
//        val builder = AlertDialog.Builder(this)
//        // add a list
//        val animals = arrayOf("Starter", "Main course", "Dessert", "Drink")
//        builder.setItems(animals) { dialog, which ->
//            when (which) {
//                0 -> { /* Starter */
//                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
//                        "STARTER_FOOD",
//                        "0",
//                        "STARTER",
//                        0
//                    )
//                    ))
//                }
//                1 -> { /* Main course  */
//                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
//                        "MAIN_COURSE_FOOD",
//                        "0",
//                        "MAIN_COURSE",
//                        0
//                    )
//                    ))
//                }
//                2 -> { /* Dessert */
//                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
//                        "DESSERT_FOOD",
//                        "0",
//                        "DESSERT",
//                        0
//                    )
//                    ))
//                }
//                3 -> { /* Drink */
//                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
//                        "DRINK_FOOD",
//                        "0",
//                        "DRINK",
//                        0
//                    )
//                    ))
//                }
//            }
//        }
//
//        // create and show the alert dialog
//        val dialog = builder.create()
//        dialog.show()


        val ract = locateView(view)
        categoryPopup = showAlertFilter()
        categoryPopup?.isOutsideTouchable = true
        categoryPopup?.isFocusable = true
        categoryPopup?.setBackgroundDrawable(getDrawable(R.drawable.add_remove_bg))
        categoryPopup!!.showAtLocation(view, Gravity.CENTER, 0, ract!!.left)
//        filterPopup?.showAsDropDown(filter)
    }

    private fun showAlertFilter(): PopupWindow {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(com.example.foodsampleapp.R.layout.menu_popup_layout, null)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
//        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
        categoryAdapter = CategoryAdapter(this,categoryItems,this)
        recyclerView.adapter = categoryAdapter
        return PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun locateView(v: View?): Rect? {
        val loc_int = IntArray(2)
        if (v == null)
            return null
        try {
            v.getLocationOnScreen(loc_int)
        } catch (npe: NullPointerException) {
            return null
        }

        val location = Rect()
        location.left = loc_int[0]
        location.top = loc_int[1]
        location.right = loc_int[0] + v.width
        location.bottom = loc_int[1] + v.height
        return location
    }

    override fun onClick(view: View?) {

        val position:Int = view?.tag as Int
        Log.d("Clicked", "yes")
        Log.d("Position Clicked: ", ""+position)

        when (view?.id){
            R.id.btn_add -> {
                Log.d("Button Clicked", "Add")
                var item = foodItems.get(position)
                item.orderQty = item.orderQty?.plus(1)
                foodItemViewModel.update(item)
            }

            R.id.txt_minus ->{
                Log.d("Button Clicked", "Minus")
                var item = foodItems.get(position)
                item.orderQty = item.orderQty?.minus(1)
                foodItemViewModel.update(item)
            }

            R.id.txt_plus ->{
                Log.d("Button Clicked", "Plus")
                var item = foodItems.get(position)
                item.orderQty = item.orderQty?.plus(1)
                foodItemViewModel.update(item)
            }

            R.id.ll_category_row->{
                categoryItems.iterator().forEach {
                    it.selected = false
                }
                categoryItems[position].selected = true
                categoryAdapter.notifyDataSetChanged()
                when (position) {
                0 -> { /* Starter */
                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
                        "STARTER_FOOD",
                        "0",
                        "STARTER",
                        0
                    )
                    ))
                }
                1 -> { /* Main course  */
                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
                        "MAIN_COURSE_FOOD",
                        "0",
                        "MAIN COURSE",
                        0
                    )
                    ))
                }
                2 -> { /* Dessert */
                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
                        "DESSERT_FOOD",
                        "0",
                        "DESSERT",
                        0
                    )
                    ))
                }
                3 -> { /* Drink */
                    recyclerView.smoothSnapToPosition(foodItems.indexOf(element = FoodItem(
                        "DRINK_FOOD",
                        "0",
                        "DRINK",
                        0
                    )
                    ))
                }
            }
            }
        }
    }

    fun updateCartCount(count:Int){
        bottom_view_cart.text = resources.getString(R.string.cart_count,count);
    }

    fun RecyclerView.smoothSnapToPosition(position: Int, snapMode: Int = LinearSmoothScroller.SNAP_TO_START) {
        val smoothScroller = object: LinearSmoothScroller(this.context) {
            override fun getVerticalSnapPreference(): Int {
                return snapMode
            }

            override fun getHorizontalSnapPreference(): Int {
                return snapMode
            }
        }
        smoothScroller.targetPosition = position
        layoutManager?.startSmoothScroll(smoothScroller)
    }
}
