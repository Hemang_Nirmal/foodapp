package com.example.foodsampleapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.PopupWindow
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.foodsampleapp.R
import com.example.foodsampleapp.viewModel.FoodItemViewModel
import com.example.foodsampleapp.adapter.FoodAdapter
import com.example.foodsampleapp.model.Categories
import com.example.foodsampleapp.model.FoodItem
import kotlinx.android.synthetic.main.activity_my_cart.*

class MyCartActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: FoodAdapter
    val foodItems: ArrayList<FoodItem> = ArrayList()
    val categoryItems: ArrayList<Categories> = ArrayList()
    private lateinit var foodItemViewModel: FoodItemViewModel
    private var categoryPopup: PopupWindow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_cart)

        toolbar_layout.setTitleEnabled(false);
        toolbar_title.text ="My Cart";
        foodItemViewModel = ViewModelProviders.of(this).get(FoodItemViewModel::class.java)

        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = linearLayoutManager
        adapter = FoodAdapter(foodItems, this)
        recyclerView.adapter = adapter

        foodItemViewModel.allOrderedItems.observe(this, Observer { orders ->
            orders?.let {
                Log.d("Order Observer Clicked","Observer called")

                if(it.size>2){
                    txt_show.visibility = View.VISIBLE
                    if(txt_show.text.equals("SHOW MORE")){
                        FoodAdapter.SHOW_LESS = true
                    }
                }else{
                    txt_show.visibility = View.GONE
                    txt_show.text = "SHOW MORE"
                    FoodAdapter.SHOW_LESS = false
                }

                foodItems.clear()
                foodItems.addAll(it)
                adapter.notifyDataSetChanged()
                var ammount:Int = 0
                orders.iterator().forEach {
                    val price = it.price?.toInt()
                    if (price != null) {
                        ammount = ammount.plus(price)
                    }
                }
                updateTotalAmmount(ammount)
            }
        })

        btn_back.setOnClickListener(View.OnClickListener {
            FoodAdapter.SHOW_LESS = false
            this.finish()
        })

        txt_show.setOnClickListener(View.OnClickListener {
            if(txt_show.text.equals("SHOW MORE")){
                FoodAdapter.SHOW_LESS = false
                txt_show.text = "SHOW LESS"
                adapter.notifyDataSetChanged()
            }else{
                FoodAdapter.SHOW_LESS = true
                txt_show.text = "SHOW MORE"
                adapter.notifyDataSetChanged()
            }
        })
    }

    override fun onBackPressed() {
        FoodAdapter.SHOW_LESS = false
        super.onBackPressed()
    }



    private fun updateTotalAmmount(count: Int) {
        txt_total_cost.text = "$"+count
    }

    override fun onClick(view: View?) {

        val position:Int = view?.tag as Int
        Log.d("Clicked", "yes")
        Log.d("Position Clicked: ", ""+position)

        when (view?.id){
            R.id.btn_add -> {
                Log.d("Button Clicked", "Add")
                var item = foodItems.get(position)
                item.orderQty = item.orderQty?.plus(1)
                foodItemViewModel.update(item)
            }

            R.id.txt_minus ->{
                Log.d("Button Clicked", "Minus")
                var item = foodItems.get(position)
                item.orderQty = item.orderQty?.minus(1)
                foodItemViewModel.update(item)
            }

            R.id.txt_plus ->{
                Log.d("Button Clicked", "Plus")
                var item = foodItems.get(position)
                item.orderQty = item.orderQty?.plus(1)
                foodItemViewModel.update(item)
            }
        }
    }
}
