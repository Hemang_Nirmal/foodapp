package com.example.foodsampleapp.repository

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.foodsampleapp.App
import com.example.foodsampleapp.model.FoodItem
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [FoodItem::class], version = 1)
public abstract class FoodDatabase : RoomDatabase() {
    abstract fun foodItemDao(): FoodItemDao

    companion object {
        @Volatile
        private var INSTANCE: FoodDatabase? = null
        private var appContext: Context? = null

        fun getDatabase(context: Context, scope: CoroutineScope): FoodDatabase {
            val tempInstance = INSTANCE
            appContext = context.applicationContext
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FoodDatabase::class.java,
                    "Word_database"
                ).addCallback(DatabaseCallback(scope)).build()
                INSTANCE = instance
                return instance
            }
        }


        private class DatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            /**
             * Override the onOpen method to populate the database.
             * For this sample, we clear the database every time it is created or opened.
             */
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                // If you want to keep the data through app restarts,
                // comment out the following line.
                INSTANCE?.let { database ->
                    scope.launch {
                        if(!App.prefs!!.isDbPopulated)
                            populateDatabase(database.foodItemDao())
                    }
                }
            }
        }

        /**
         * Populate the database in a new coroutine.
         * If you want to start with more words, just add them.
         */
        suspend fun populateDatabase(foodItemDao: FoodItemDao) {
            val file_name = "food.json"
            val json_string = appContext?.assets?.open(file_name)?.bufferedReader().use {
                var str = it?.readText()
                val gson = Gson()
                val foodItemList: List<FoodItem> = gson.fromJson(str, Array<FoodItem>::class.java).toList()
                Log.d("Data", "Paresed")
                val iterator = foodItemList.iterator()
                iterator.forEach {
                    foodItemDao.insert(it)
                }
                Log.d("Data", "Data inserted in Database")
                App.prefs!!.isDbPopulated = true
            }
        }
    }


}