package com.example.foodsampleapp.repository

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.foodsampleapp.model.FoodItem

@Dao
interface FoodItemDao {
    @Query("SELECT * from food_table")
    fun getAllfooditems(): LiveData<List<FoodItem>>

    @Query("SELECT * from food_table where orderQty>0")
    fun getAllOrderedItems(): LiveData<List<FoodItem>>

    @Insert
    suspend fun insert(foodItem: FoodItem)

    @Update
    suspend fun update(foodItem: FoodItem)

    @Query("SELECT count(*) from food_table where foodType LIKE :search")
    fun getCategoryCount(search:String): LiveData<Int>
}