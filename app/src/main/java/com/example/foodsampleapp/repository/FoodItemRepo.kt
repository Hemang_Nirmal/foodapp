package com.example.foodsampleapp.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.foodsampleapp.model.FoodItem

class FoodItemRepo(private val foodDao: FoodItemDao) {

    val allFooditems:LiveData<List<FoodItem>> = foodDao.getAllfooditems()
    val allOrderedItems:LiveData<List<FoodItem>> = foodDao.getAllOrderedItems()
    val mainCount:LiveData<Int> = foodDao.getCategoryCount("main")
    val starterCount:LiveData<Int>  = foodDao.getCategoryCount("starter")
    val dessertCount:LiveData<Int>  = foodDao.getCategoryCount("Dessert")
    val drinkCount:LiveData<Int>  = foodDao.getCategoryCount("drink")

    @WorkerThread
    suspend fun insert(foodItem: FoodItem){
        foodDao.insert(foodItem)
    }

    @WorkerThread
    suspend fun update(foodItem: FoodItem){
        foodDao.update(foodItem)
    }

    @WorkerThread
    suspend fun getCategoryCount(search:String){
        foodDao.getCategoryCount(search)
    }

}