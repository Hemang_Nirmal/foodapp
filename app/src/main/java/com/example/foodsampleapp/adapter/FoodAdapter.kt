package com.example.foodsampleapp.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.foodsampleapp.model.FoodItem
import com.example.foodsampleapp.R
import kotlinx.android.synthetic.main.recyclerview_row.view.*

class FoodAdapter(private val foodItems: ArrayList<FoodItem>, val onClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val SECTION_VIEW = 0
        const val CONTENT_VIEW = 1
        var POSITION_MAIN = 0
        var POSITION_STARTER = 0
        var POSITION_DESSERT = 0
        var POSITION_DRINK = 0
        var SHOW_LESS = false
    }

//    val parentClicklistener: View.OnClickListener = onClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val inflatedView: View
        if (viewType == CONTENT_VIEW) {
            inflatedView = inflater.inflate(R.layout.recyclerview_row, parent, false)
            return ViewHolder(inflatedView)
        } else {
            inflatedView = inflater.inflate(R.layout.recycler_section, parent, false)
            return SectionViewHolder(inflatedView)
        }
    }

    override fun getItemCount(): Int {
        if(SHOW_LESS){
            return 2
        }
        return foodItems.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = foodItems[position]
        if (holder is ViewHolder) {
            holder.bindViews(item, position)
        } else if (holder is SectionViewHolder) {
            holder.bindViews(item,position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (foodItems[position].foodType.equals("MAIN COURSE") || foodItems[position].foodType.equals("STARTER")
            || foodItems[position].foodType.equals("DESSERT") || foodItems[position].foodType.equals("DRINK")
        ) {
            return SECTION_VIEW
            Log.d("Section :", "true")
        } else {
            return CONTENT_VIEW
            Log.d("Content :", "true")
        }
    }

    //1
    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        //2
        private var foodItem: FoodItem? = null
        private var position: Int? = null

        //3
//        init {
//            view.setOnClickListener(this)
//        }

        fun bindViews(foodItem: FoodItem, position: Int) {
            this.foodItem = foodItem
            this.position = position
            view.txt_title.text = foodItem.foodName
            view.txt_price.text = "$"+foodItem.price
            if (foodItem.orderQty!! > 0) {
                view.group.visibility = View.VISIBLE
                view.btn_add.visibility = View.GONE

            } else {
                view.group.visibility = View.GONE
                view.btn_add.visibility = View.VISIBLE
            }
            view.txt_count.text = foodItem.orderQty.toString()
            view.btn_add.setOnClickListener(this)
            view.txt_minus.setOnClickListener(this)
            view.txt_plus.setOnClickListener(this)
        }

        //4
        override fun onClick(v: View) {
            v.tag = position
            onClickListener.onClick(v)

        }
    }


    class SectionViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private var foodItem: FoodItem? = null

        fun bindViews(foodItem: FoodItem, position: Int) {
            this.foodItem = foodItem
            view.txt_title.text = foodItem.foodType

            when (foodItem.foodType) {
                "MAIN COURSE" -> { /*  Main course */
                    POSITION_MAIN = position
                }
                "STARTER" -> { /* Starter */
                    POSITION_STARTER = position
                }
                "DESSERT" -> { /* Dessert */
                    POSITION_DESSERT = position
                }
                "DRINK" -> { /* Drink */
                    POSITION_DRINK = position
                }
            }
        }
    }
}