package com.example.foodsampleapp.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.foodsampleapp.R
import com.example.foodsampleapp.model.Categories
import kotlinx.android.synthetic.main.categories_row.view.*

class CategoryAdapter(private val context: Context,private val items: ArrayList<Categories>, val onClickListener: View.OnClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//    companion object {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val inflatedView: View
        inflatedView = inflater.inflate(R.layout.categories_row, parent, false)
        return ViewHolder(inflatedView)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        if (holder is ViewHolder) {
            holder.bindViews(item, position)
        }
    }

    //1
    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        //2
        private var item: Categories? = null
        private var position: Int? = null

        //3
        init {
            view.setOnClickListener(this)
        }

        fun bindViews(item: Categories, position: Int) {
            this.item = item
            this.position = position
            view.txt_category_name.text = item.name.toString()
            view.txt_count.text =  item.count.toString()
            if(item.selected){
                view.txt_category_name.setTextColor(Color.BLACK)
                view.txt_category_name.setTypeface(null,Typeface.BOLD)
            }else{
                view.txt_category_name.setTextColor(Color.DKGRAY)
                view.txt_category_name.setTypeface(null,Typeface.NORMAL)
            }
        }

        //4
        override fun onClick(v: View) {
            v.tag = position
            onClickListener.onClick(v)

        }
    }
}