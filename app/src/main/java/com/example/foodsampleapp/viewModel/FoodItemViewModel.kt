package com.example.foodsampleapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.foodsampleapp.model.FoodItem
import com.example.foodsampleapp.repository.FoodDatabase
import com.example.foodsampleapp.repository.FoodItemRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FoodItemViewModel(application: Application):AndroidViewModel(application) {
    private val repository: FoodItemRepo
    val allFoodItems: LiveData<List<FoodItem>>
    val allOrderedItems: LiveData<List<FoodItem>>
    val mainCount:LiveData<Int>
    val starterCount:LiveData<Int>
    val dessertCount:LiveData<Int>
    val drinkCount:LiveData<Int>



    init {
        val foodItemDao = FoodDatabase.getDatabase(application,viewModelScope).foodItemDao()
        repository = FoodItemRepo(foodItemDao)
        allFoodItems = repository.allFooditems
        allOrderedItems = repository.allOrderedItems
        mainCount = repository.mainCount
        starterCount = repository.starterCount
        dessertCount = repository.dessertCount
        drinkCount = repository.drinkCount
    }

    fun insert(foodItem: FoodItem) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(foodItem)
    }

    fun update(foodItem: FoodItem) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(foodItem)
    }

}